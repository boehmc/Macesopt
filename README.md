
# Macesopt


![Macesopt](macesopt.png)

The Macesopt package offers a flexible framework to simulate
the propagation of seismic waves in solid and fluid medium and to
solve related tomography problems. 

Macesopt consists of four components: 
- a container to store seismic sources and observed data, 
- a wave simulation code, 
- an intermediate layer that stores the material mesh, and 
- optimization routines to solve the inverse problem.

The code has been developed to simulate the acoustic and the elastic wave
equation in two and three dimensions. We also
consider the case of wave propagation in a coupled system of fluid and
acoustic medium in 3d.

The optimization toolbox comes with a collection of methods to solve
the seismic inverse problem, including line-search and trust-region
globalizations, quasi-Newton approximations of the Hessian and a Newton-PCG
methods, as well as the Moreau-Yosida regularization and projection methods to 
handle additional constraints on the material parameters.

Efficient inversion methods rely on a scalable
code for the simulation of the elastic-acoustic wave equation.
The implementation is
parallelized, utilizes MPI-communication and works matrix-free.
Moreover, it is not required to solve a linear system during the simulation, since an
explicit Newmark time-stepping scheme and a diagonal mass matrix
are used. 
Forward and adjoint simulations are carried out to efficiently compute
the reduced gradient and reduced Hessian-vector products that are required by the
Newton-CG method

---

Access to the code is granted upon request:

christian.boehm@erdw.ethz.ch